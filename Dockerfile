FROM node:14.17-alpine3.13

RUN apk update
RUN apk add yarn

WORKDIR app

COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn install

COPY . .

RUN yarn migrate:latest

EXPOSE 3000

CMD ["yarn", "start"]
