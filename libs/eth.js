// A wrapper for web3
import Web3 from "web3";
import config from "../config.js";

// deprecate?
//let httpWeb3 = new Web3(`https://mainnet.infura.io/v3/${config.dev.PROJECT_ID}`);

export function subscribeNewBlocks(wsWeb3, dataCallback) {
  var subscription = wsWeb3.eth
    .subscribe("newBlockHeaders", dataCallback)
    .on("connected", function (subscriptionId) {
      console.log("connected: ", subscriptionId);
    })
    /*.on("data", (blockHeader) => {
      console.log("ldata")
      //dataCallback(blockHeader)
    })
    */
    .on("error", console.error);

  return subscription;
}

export function subscribePendingTxs(wsWeb3, httpWeb3) {
  var subscription = wsWeb3.eth
    .subscribe("pendingTransactions")
    .on("connected", function (subscriptionId) {
      console.log("connected: ", subscriptionId);
    })
    .on("data", async function (txHash) {
      //const tx = await httpWeb3.eth.getTransaction(txHash);
      const tx = await wsWeb3.eth.getTransaction(txHash);

      console.log("txhash: ", txHash);
      console.log("tx: ", tx);
      console.log("");
    })
    .on("error", console.error);

  subscription.unsubscribe((err, success) => {
    if (success) {
      console.log("Successfully unsubscribed");
    }
  });
}

export async function getTransaction(txHash) {
  const tx = await httpWeb3.eth.getTransaction(txHash);

  console.log("txhash: ", txHash);
  console.log("tx: ", tx);
  console.log("");
}
