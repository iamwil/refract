exports.up = function (knex) {
  return knex.schema.createTable("annual_percentage_yields", function (table) {
    table.increments();
    table.bigInteger("blocknumber");
    table.string("project_name");
    table.jsonb("data");
    table.timestamp("block_at");
    table.timestamps(true, true);
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("annual_percentage_yields");
};
