exports.up = function (knex) {
  return knex.schema.table("annual_percentage_yields", function (table) {
    table.string("market_name");
  });
};

exports.down = function (knex) {
  return knex.schema.table("annual_percentage_yields", function (table) {
    table.dropColumn("market_name");
  });
};
