exports.up = function (knex) {
  return knex.schema.table("annual_percentage_yields", function (table) {
    table.string("version").defaultTo("0");
  });
};

exports.down = function (knex) {
  return knex.schema.table("annual_percentage_yields", function (table) {
    table.dropColumn("version");
  });
};
