import * as fs from "fs/promises";
import Knex from "knex";
const Web3 = require("web3");

import { Refract, Component, Fiber } from "./refract";

/******* custom hooks ******/

export const useAsyncEffect = (name, asyncCallback, deps) => {
  Refract.useEffect(name, () => {
    (async () => { asyncCallback() })()
  }, deps)
}

export const useFetchJson = filePath => {
  let [json, setJson] = Refract.useState("useFetchJson", null);

  Refract.useEffect(
    "useFetchJson",
    () => {
      (async () => {
        console.log(`loading ${filePath}...`);
        const result = await fs.readFile(filePath);
        // TODO use simdjson
        const parsed = JSON.parse(String(result));
        setJson(parsed);
        console.log(`loaded ${filePath}`);
      })();
    },
    [filePath]
  );

  return json;
};

export const useMissingKeys = (
  knex,
  tableName: string,
  keyOfRecordName: string,
  keyOfRecord: number,
  options: { numPastKeys?: number } = {}
) => {
  let { numPastKeys } = options;
  numPastKeys ||= 127;

  let [missingKeys, setMissingKeys] = Refract.useState("missing keys", []);

  let [isQuerying, setIsQuerying] =  Refract.useState("is querying missing keys", false)

  let [numRemainingKeys, setNumRemainingKeys] = Refract.useState(
    "num keys to backfill",
    0
  );

  Refract.useEffect(
    "find missing",
    () => {
      if (!knex || !keyOfRecord) return;

      // Can't just use numRemainingKeys to lock, because it only gets set
      // after the query comes back. In the interim, another blockheader could
      // come in.
      if (isQuerying) return;

      if (numRemainingKeys !== 0) return;

      (async () => {
        // Need to lock the query here before querying, not after
        setIsQuerying(true)

        const res = await knex.raw("? except ? order by missing_key asc", [
          knex.raw(`select generate_series(
              (${keyOfRecord - 127}),
              (${keyOfRecord - 1})
            ) as missing_key`),

          knex
            .select(keyOfRecordName)
            .from(tableName)
            .whereBetween(keyOfRecordName, [keyOfRecord - numPastKeys, keyOfRecord - 1])
        ]);
        console.log(
          `Backfilling from ${keyOfRecord - numPastKeys} to ${keyOfRecord}: ${
            res.rows.length
          } blocknumbers`
        );

        setMissingKeys(res.rows.map(row => row["missing_key"]));
        setNumRemainingKeys(res.rows.length);
        setIsQuerying(false)
      })();
    },
    [knex, keyOfRecord, numRemainingKeys, numPastKeys]
  );

  return { missingKeys, numRemainingKeys, setNumRemainingKeys };
};

export const useWeb3 = (providerUri: string) => {
  let [web3, setWeb3] = Refract.useState("useWeb3", null);

  let [isReconnect, setReconnect] = Refract.useState("useConnect", false);

  Refract.useEffect(
    "useWeb3",
    () => {
      console.log("connecting to web3 provider...");
      setWeb3(new Web3(providerUri));
      setReconnect(false);
      console.log(`connected to web3 ${providerUri}`);
    },
    [providerUri, isReconnect]
  );

  return [web3, setReconnect];
};

export const useCompoundContract =
  (web3, mainnetAbi, mainnet) => cTokenName => {
    let [contract, setContract] = Refract.useState("useCompoundContract", null);

    Refract.useEffect(
      "useCompoundContract",
      () => {
        if (!web3 || !mainnetAbi || !mainnet) return;

        const contract = new web3.eth.Contract(
          mainnetAbi[cTokenName],
          mainnet.Contracts[cTokenName]
        );

        setContract(contract);
        console.log(`contract created for ${cTokenName}`);
      },
      [web3, mainnet, mainnetAbi, cTokenName]
    );

    return contract;
  };

// Not yet ready for primetime
export const useSubscribe = (
  web3,
  setIsConnect: (any) => void,
  timeout: number
) => {
  let [result, setResult] = Refract.useState("useSubscribe", null);

  let timer;
  let subscription;

  const resubscribe = () => {
    console.log(
      `!!!!!!!!!!!!!!!!!! subscription expired. resubscribing...`,
      !!subscription
    );

    if (subscription) {
      subscription.unsubscribe((err, isSuccess) => {
        if (isSuccess) {
          console.log("unsubscribed");
        } else {
          console.log("unsub err", err);
          setIsConnect(false);
        }
      });
    }

    subscription = web3.eth.subscribe("newBlockHeaders");

    subscription.on("connected", subscriptionId => {
      console.log("sub conn success", subscriptionId);
    });

    subscription.on("data", res => {
      setResult(res);
      console.log("reset resubscription timeout", !!timer);
      if (timer) clearTimeout(timer);
      timer = setInterval(resubscribe, timeout);
    });
  };

  Refract.useEffect(
    "useSubscribe",
    () => {
      if (!web3) return;

      console.log(`effect: subscribing...`);

      resubscribe();
      timer = setInterval(resubscribe, timeout);
    },
    [web3]
  );

  return result;
};

export const useBlockHeader = (web3, setReconnect) => {
  let [blockHeader, setBlockHeader] = Refract.useState("useBlockHeader", null);

  let [subscription, setSubscription] = Refract.useState(
    "useBlockheader Sub",
    null
  );

  Refract.useEffect(
    "useBlockHeader",
    () => {
      if (!web3) return;

      console.log("subscribing to block headers...");

      if (subscription) {
        subscription.unsubscribe((err, isSuccess) => {
          if (isSuccess) {
            console.log("unsubscribed");
          } else {
            console.log("unsub err", err);
            setReconnect(false);
          }
        });
      }

      const sub = web3.eth
        .subscribe("newBlockHeaders")
        .on("data", blockHeader => {
          setBlockHeader(blockHeader);
          console.log("new block...", blockHeader);
        });
      setSubscription(sub);
    },
    [web3]
  );

  return blockHeader;
};

export const useBorrowAndSupplyRate = (contract, blocknumber) => {
  let [borrowRate, setBorrowRate] = Refract.useState("useRates", null);
  let [supplyRate, setSupplyRate] = Refract.useState("useRates", null);

  Refract.useEffect(
    "useRates",
    () => {
      if (!contract || !blocknumber) return;

      (async function () {
        //console.log("getting APY rates...");
        const [brate, srate] = await Promise.all([
          contract.methods.borrowRatePerBlock().call({}, blocknumber),
          contract.methods.supplyRatePerBlock().call({}, blocknumber)
        ]);

        setBorrowRate(brate);
        setSupplyRate(srate);
        //console.log("APY rates fetched");
      })();
    },
    [contract, blocknumber]
  );

  return [borrowRate, supplyRate];
};

export const useKnex = dbConfig => {
  let [knex, setKnex] = Refract.useState("useKnex", null);

  Refract.useEffect(
    "useKnex",
    () => {
      const kx = Knex(dbConfig);
      console.log(`Set knex configuration`);
      setKnex(kx);
    },
    []
  );

  return knex;
};

// Persist to the db.
// - keys: how to identify a unique record. If these are all the same, the persistance
// doesn't run.
//
// TODO Should switch to an upsert
// TODO need to implement error handling and retries
// TODO would this be a candidate effect for "rendering"?
export const usePersist = (knex, record, keys) => {
  let values = keys.map(key => record && record[key]);

  Refract.useEffect(
    "usePersist",
    () => {
      if (!knex || !record) return;

      (async () => {
        await knex("annual_percentage_yields").insert(record);
        console.log(`inserted record:`, record.blocknumber, record.market_name);
      })();
    },
    values
  );
};
