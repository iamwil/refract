import "source-map-support/register";
import { Refract } from "./refract";
import * as config from "../config";

const TOKEN_LIST_PATH = "./tokenlists/compound.tokenlist.json";
const MAINNET_PATH = "./abis/compound-mainnet.json";
const MAINNET_ABI_PATH = "./abis/compound-mainnet-abi.json";
const PROVIDER_URI = `wss://mainnet.infura.io/ws/v3/${config.dev.PROJECT_ID}`;

import { ApyIngestor, MainLine } from "./ingestor";

(async function() {
  let App = Refract.render(ApyIngestor, {
    key: "0",
    providerUri: PROVIDER_URI,
    tokenListPath: TOKEN_LIST_PATH,
    mainnetPath: MAINNET_PATH,
    mainnetAbiPath: MAINNET_ABI_PATH
  });
})().then(() => {
  console.log("done");
}).catch(err => {
  console.error("top level error", err)
});

process.on('uncaughtException', (err, origin) => {
  console.error(`Uncaught Exception (${origin}): ${err.message}`)
  console.error(err.stack)
}).on('unhandledRejection', (reason, promise) => {
  console.error(`Unhandled Rejection: ${reason}`)
})
