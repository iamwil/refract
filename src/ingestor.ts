import * as config from "../config";

import { Refract, elem, ElementProps, ComponentProps } from "./refract";
import {
  useFetchJson,
  useWeb3,
  useCompoundContract,
  useBlockHeader,
  useSubscribe,
  useBorrowAndSupplyRate,
  useKnex,
  usePersist,
  useMissingKeys
} from "./hooks";

/******* components *******/

export const ApyIngestor = props => {
  let { providerUri, tokenListPath, mainnetPath, mainnetAbiPath } = props;

  let [web3, setReconnect] = useWeb3(providerUri);

  const knex = useKnex(config.dev.db);
  const tokenList = useFetchJson(tokenListPath);

  const mainnet = useFetchJson(mainnetPath);
  const mainnetAbi = useFetchJson(mainnetAbiPath);

  let contractGenerator = useCompoundContract(web3, mainnetAbi, mainnet);

  const filteredTokens =
    tokenList?.tokens
      ?.filter(tokenRecord => tokenRecord.chainId === 1)
      ?.filter(tokenRecord => tokenRecord.symbol.match(/c.*/)) || [];

  let blockHeader = useBlockHeader(web3, setReconnect);
  //let blockHeader = useSubscribe(web3, setIsConnect, 30000);

  return [
    elem(MainLine, {
      key: "0",
      knex,
      tokens: filteredTokens,
      contractGenerator,
      blockHeader
    }),

    elem(Backfill, {
      key: "1",
      web3,
      knex,
      tokens: filteredTokens,
      contractGenerator,
      latestBlockHeader: blockHeader
    })
  ];
};

export const MainLine = props => {
  let { knex, tokens, contractGenerator, blockHeader } = props;

  return [
    elem(
      "mainline_data",
      {
        key: "0",
        filteredTokens: tokens?.length,
        blockHeader: blockHeader?.number
      },
      () => {
        return tokens.map((tokenRecord, i) => {
          return elem(TokenRates, {
            key: i + 1,
            knex,
            tokenRecord,
            contract: contractGenerator(tokenRecord.symbol),
            blockHeader
          });
        });
      }
    )
  ];
};

export const Backfill = props => {
  let { web3, knex, tokens, contractGenerator, latestBlockHeader, children } =
    props;

  const { missingKeys, numRemainingKeys, setNumRemainingKeys } = useMissingKeys(
    knex,
    "annual_percentage_yields",
    "blocknumber",
    latestBlockHeader?.number
  );

  let [pastBlockHeader, setPastBlockHeader] = Refract.useState(
    "past blockheader",
    null
  );

  // shouldn't dep on remaining keys because otherwise, this will execute
  // every time a missing key is processed
  Refract.useEffect(
    "get past block",
    () => {
      if (!web3 || !missingKeys) return;

      if (missingKeys.length === 0) return;

      (async () => {
        console.log("missing keys", missingKeys);

        missingKeys.forEach(async blocknumber => {
          const bh = await web3.eth.getBlock(blocknumber);
          setPastBlockHeader(bh);
          setNumRemainingKeys(numRemainingKeys - 1);
        });
      })();
    },
    [missingKeys]
  );

  return [
    elem(MainLine, {
      key: "0",
      knex,
      tokens,
      contractGenerator,
      blockHeader: pastBlockHeader
    })
  ];
};

const TokenRates = props => {
  let { knex, tokenRecord, contract, blockHeader } = props;

  const [borrowRate, supplyRate] = useBorrowAndSupplyRate(
    contract,
    blockHeader?.number
  );

  // TODO it'd help to have declarative guards. Use Typescript?
  let record: AnnualPercentageYieldRecord;
  if (blockHeader && tokenRecord && borrowRate && supplyRate) {
    record = {
      project_name: "compound",
      blocknumber: blockHeader.number,
      market_name: tokenRecord.symbol,
      data: {
        borrow_rate: borrowRate,
        supply_rate: supplyRate
      },
      block_at: new Date(blockHeader.timestamp * 1000),
      created_at: new Date(),
      updated_at: new Date()
    };
  }

  usePersist(knex, record, ["project_name", "blocknumber", "market_name"]);

  return [elem("token_data", { key: "0", record })];
};

export type AnnualPercentageYieldRecord = {
  project_name: string;
  blocknumber: number;
  market_name: string;
  data: {
    borrow_rate: string;
    supply_rate: string;
  };
  block_at: Date;
  created_at: Date;
  updated_at: Date;
};
