import { EventEmitter } from "events";

/******* runtime *******/

export type Component = (props: ComponentProps) => Element[];

// TODO should it define children and childrenCb?
export type ComponentProps = {
  [key: string]: any;
  key: string;
};

export enum EffectType {
  INSTANCIATE,
  UPDATE_STATE_AND_PROPS,
  USE_STATE,
  USE_EFFECT
}

export type Effect = {
  name: string;
  type: EffectType;
  callback: EffectCallback;
};

export type EffectCallback = (fiber: Fiber) => void | (() => void);

export enum DispatchMode {
  EFFECT,
  UNIT_OF_WORK
}

export type Element = {
  name: string;
  component: Component;
  isAtomic: boolean;
  props: ElementProps;
  children: Element[] | null; // null means childrenCb hadn't been called yet
  childrenCb: () => Element[];
};

export type ElementProps = {
  [key: string]: any;
  key: string;
};

export class Fiber {
  idx: number; // index tracking which hookState we're working on
  hookStates: any[];

  dispatchMode: DispatchMode;

  // any event from user or callback from source gets dispatched here
  effectQueue: Effect[];

  // any work that needs to be done as a result of processing a single effect
  workQueue: Effect[];

  name: string;
  component: Component;
  isAtomic: boolean; // should technically be a property on the component
  props: ComponentProps;
  pendingProps: ComponentProps;

  instances: { [key: string]: Element };

  childrenCb: () => Element[] | null;
  children: { [key: string]: Fiber };

  constructor(instance: Element) {
    this.idx = 0;
    this.hookStates = [];

    this.dispatchMode = DispatchMode.EFFECT;
    this.effectQueue = [];
    this.workQueue = [];

    this.updateFiber(instance);

    this.children = {}; // the list of fiber children
  }

  updateFiber(instance: Element) {
    this.name = instance.name || null;
    this.component = instance.component;
    this.isAtomic = instance.isAtomic;
    this.instances = {}; // these are instances when instanciating the component
    this.props = { key: "-" }; // (not being used) props from last committed state
    this.pendingProps = instance.props;

    this.childrenCb = instance.childrenCb; // callback for instance's children
  }

  // queues up hook work and calculates the instanciated state of component instance
  // has to be synchronous, so dispatch mode works correctly
  instanciateComponent(): Element[] {
    //console.log(`  **** ${this.name}: instanciating component ****`);
    this.idx = 0;

    // Dispatch to work queue
    this.dispatchMode = DispatchMode.UNIT_OF_WORK;

    //console.log(
    //  `    ${this.name} pending props:`,
    //  Object.keys(this.pendingProps)
    //);

    // Instanciate the current component, which queues up all the work
    // that the hooks generate. Instanciate gives an array of instances
    const propsWithChildren = {
      ...this.pendingProps,
      children: (this.childrenCb && this.childrenCb()) || []
    };
    //console.log(
    //  `    ${this.name} pending prop children length:`,
    //  propsWithChildren.children.length
    //);

    let newInstances;
    if (!this.isAtomic) {
      newInstances = this.component(propsWithChildren);
      //console.log(
      //  `    ${this.name} is component instance--running component yielded ${newInstances.length} instances`
      //);
    } else {
      const atomicInstances = this.component(propsWithChildren);
      const atomicInstance = atomicInstances[0];
      newInstances =
        (atomicInstance.childrenCb && atomicInstance.childrenCb()) || [];

      //console.log(
      //  `      ${this.name}: is an atomic instance--running its children yielded ${newInstances.length} instances`
      //);
    }

    for (const newInstance of newInstances) {
      //console.log(`      ${this.name}.${newInstance.name}: new instances`);
      //console.log(
      //  `      childrenCb the same?:`,
      //  this.instances[newInstance.props.key!]?.childrenCb ===
      //    newInstance?.childrenCb
      //);

      // for component elements, we just add the new instances to instance array
      this.instances[newInstance.props.key!] = newInstance;

      //console.log(
      //  `      ${this.name}.${newInstance.name}: added as fiber's instances`
      //);
      //console.log(`      props:`, Object.keys(newInstance.props));
      //console.log(
      //  `      new instance's children: ${
      //    newInstance.children?.length || 0
      //  }, new instance's childrenCb: ${!!newInstance.childrenCb}`
      //);
    }

    // Dispatch to effects queue
    this.dispatchMode = DispatchMode.EFFECT;

    //console.log(`  **** ${this.name}: end instanciation ****`);

    return newInstances;
  }

  // TODO change to fiber.upsertChildFiber(key, element)
  upsertChildFiber(key: string, childInstance: Element): Fiber {
    if (key in this.children) {
      const fiber = this.children[key];
      fiber.updateFiber(childInstance);
      return fiber;
    }

    console.log(`  ${this.name}: creating fiber from ${childInstance.name}`);

    const newChildFiber = new Fiber(childInstance);
    this.setChildFiber(key, newChildFiber);

    return newChildFiber;
  }

  getChildInstance(key: string): Element {
    return this.instances[key];
  }

  updateProps(newProps): void {
    this.pendingProps = newProps;
  }

  setChildFiber(key: string = "_", childFiber: Fiber): void {
    this.children[key] = childFiber;
  }

  // shallow comparison of props
  isSameProps(): boolean {
    return (
      Object.keys(this.props) === Object.keys(this.pendingProps) &&
      Object.keys(this.props).every(key => {
        return this.props[key] === this.pendingProps[key];
      })
    );
  }

  hasPendingHooks(): boolean {
    return this.workQueue.some(effect => {
      return (
        effect.type === EffectType.USE_STATE ||
        effect.type === EffectType.USE_EFFECT
      );
    });
  }

  useState(name: string, initVal: any): [any, (any) => void] {
    const state = this.hookStates[this.idx] || initVal;
    const thisHooksIdx = this.idx; // freeze the value of idx for setState
    const setState = (newVal: any): void => {
      const before = this.effectQueue.length;

      this.dispatch({
        name,
        type: EffectType.USE_STATE,
        callback: _fib => {
          this.hookStates[thisHooksIdx] = newVal;
        }
      });
    };

    this.idx++;

    return [state, setState];
  }

  useEffect(name: string, callback: EffectCallback, deps): void {
    // NOTE do not default to [], because undefined deps should execute every run
    const oldDeps = this.hookStates[this.idx];
    let hasChanged = true;
    if (oldDeps)
      hasChanged = deps.some((dep, i) => !Object.is(dep, oldDeps[i]));

    //if (name === "get past block" && hasChanged)
    //   console.log(`!!!!!!!!!!!!!!!!!!!!! ${name} deps`, this.idx, hasChanged, oldDeps, deps);

    if (hasChanged) {
      this.dispatch({
        name,
        type: EffectType.USE_EFFECT,
        callback
      });
    }
    this.hookStates[this.idx] = deps;

    this.idx++;

    return;
  }

  dispatch(effect: Effect): void {
    this.dispatchMode === DispatchMode.EFFECT
      ? this.effectQueue.push(effect)
      : this.workQueue.push(effect);
  }
}

export const Refract = (function () {
  let currentFiberRoot = null;
  let pendingRootFiber = null;
  let currentFiber = null;

  let isWorkLoopRunning = false;

  function useState(name: string, initVal: any): [any, (any) => void] {
    return currentFiber!.useState(name, initVal);
  }

  function useEffect(
    name: string,
    callback: EffectCallback,
    deps: any[]
  ): void {
    currentFiber!.useEffect(name, callback, deps);
  }

  function dispatch(effect: Effect): void {
    currentFiber!.dispatch(effect);
  }

  function walkFiberTree(rootFiber: Fiber, callback: (Fiber) => boolean): void {
    let queue = [];
    queue.push(rootFiber);

    //console.log("======= start walk =========");

    while (queue.length !== 0) {
      currentFiber = queue.shift();
      //console.log(`====== Walk processing fiber ${currentFiber.name} ======`);

      callback(currentFiber);

      // queue to visit all children of fiber with updated props
      // NOTE couldn't use Object.entries, because it forgets the value type
      for (const key of Object.keys(currentFiber.children)) {
        const childFiber: Fiber = currentFiber.children[key];

        // we have to update props because otherwise props only get set on creation of fiber

        const childInstance = currentFiber.getChildInstance(
          childFiber.pendingProps.key // FIXME key doesn't match
        );

        // console.log(
        //   `  ${currentFiber.name}.${childFiber.name}: update props and queue for walking`
        // );

        // an array of elements
        childFiber.updateProps(childInstance.props);

        queue.push(childFiber);
      }
    }
  }

  /* NOTE it's impossible to build the fiber tree ahead of time, because you need to
   * run the hooks to figure out props, and whether certain components are included or not.
   */
  function walkAndBuildFiberTree(
    rootFiber: Fiber,
    callback: (Fiber) => boolean
  ): void {
    walkFiberTree(rootFiber, (currentFiber: Fiber) => {
      // if props aren't different, this entire branch isn't going to be different
      if (currentFiber.isSameProps()) {
        console.log("  skipping. props unchanged");
        return true;
      }

      // pop off the next effect and run it to queue up work from the effects
      const effect = currentFiber.effectQueue.shift();
      if (effect) {
        //console.log(`  running effect ${effect.name}, type ${effect.type}`);
        const cleanupCallback = effect.callback(currentFiber);
        // TODO implement cleanup
      }

      // instanciate the component to queue up work from any dependent hooks
      currentFiber.instanciateComponent();

      // do work on the current fiber. See if we can keep traversing fiber tree.
      const isResume = callback(currentFiber);
      //if (!isResume) break;

      // for each subcomponent elements of top level element, create a fiber
      for (const [key, childInstance] of Object.entries(
        currentFiber.instances
      )) {
        currentFiber.upsertChildFiber(key, childInstance);
      }
    });
  }

  function workLoop(): void {
    // TODO maybe store stats about the regularity of incoming data and adjust the frameTime.
    const frameTime = 100; //16;

    function timeLeft(startTime: number): [number, number] {
      const now = new Date().getTime();
      const elapsed = now - startTime;
      const remaining = frameTime - elapsed;

      return [elapsed, remaining];
    }

    const timer = setInterval(() => {
      const startTime = new Date().getTime();

      walkAndBuildFiberTree(currentFiberRoot, fiber => {
        //if (fiber.effectQueue.length !== 0 || fiber.workQueue.length !== 0) {
        //  console.log(
        //    `  ---- Working on fiber ${fiber.name} [ effectQueue: ${fiber.effectQueue.length} workQueue: ${fiber.workQueue.length} ] ----`
        //  );
        //}

        while (fiber.workQueue.length !== 0) {
          const unitOfWork = fiber.workQueue.shift();

          // do the work
          // TODO what to do with the return?
          const result = unitOfWork.callback(unitOfWork.fiber);

          // pause execution if there's no time left
          const [elapsed, remaining] = timeLeft(startTime);
          if (remaining <= 0) {
            //console.log(
            //  `  ---- Pausing ${fiber.name} ${remaining}ms remaining (elapsed ${elapsed}ms) effectQueue: ${fiber.effectQueue.length} | workQueue: ${fiber.workQueue.length} ----`
            //);
            return false;
          }
        }

        const [elapsed, remaining] = timeLeft(startTime);
        // console.log(
        //   `  ---- Finishing ${fiber.name} ${remaining}ms remaining (elapsed ${elapsed}ms) [ effectQueue: ${fiber.effectQueue.length}, workQueue: ${fiber.workQueue.length} ] ----`
        // );

        return true; // keep processing next fiber
      });

      // the final output isn't stable until we've run through the entire workQeueue generated from
      // a single walk of the fiber tree. Until then, we won't walk the fiber tree.
      //
      // TODO reconcile the instance? yeet reduce the instance?
      /*
      console.log("render...");
      walkFiberTree(currentFiberRoot, fiber => {
        console.log(fiber.pendingProps);
        return true;
      });
      */
    }, frameTime);
  }

  // TODO how to start the workloop with the component that's being rendered?
  function render(rootComponent: Component, rootProps: ComponentProps): void {
    currentFiberRoot = new Fiber(elem(rootComponent, rootProps));

    // start the render loop
    if (!isWorkLoopRunning) {
      isWorkLoopRunning = true;
      workLoop();
      return;
    }
  }

  return { useState, useEffect, render };
})();

function makeAtomic(name, props, childrenCb) {
  const Atomic = (props: ComponentProps): Element[] => {
    return [
      {
        name,
        component: Atomic,
        isAtomic: true,
        props,
        childrenCb: childrenCb,
        children: null
      }
    ];
  };

  return Atomic;
}

export function elem(
  componentOrName: Component | string,
  props: ElementProps,
  children: (() => Element[]) | null = null
): Element {
  let isAtomic = typeof componentOrName === "string";
  let name: string = isAtomic
    ? (componentOrName as string)
    : (componentOrName as Component).name;

  if (!props) throw new Error(`Component ${name} must have props`);

  if (!("key" in props))
    throw new Error(`Component ${name} must have a key prop`);

  let component: Component = isAtomic
    ? makeAtomic(name, props, children)
    : (componentOrName as Component);

  return {
    name,
    component,
    isAtomic,
    props,
    childrenCb: children,
    children: null
  };
}
