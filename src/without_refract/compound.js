import Web3 from "web3";
import fs from "fs/promises";
import BN from "bignumber.js";

import * as config from "../config.js";

//let wsWeb3 = new Web3(`wss://mainnet.infura.io/ws/v3/${config.PROJECT_ID}`);

// how to query for and record blocks per day?
const BLOCKS_PER_DAY = 6570;

// TODO pass in web3 instance

export async function loadTokenList() {
  const tokenListJson = await fs.readFile(
    "./tokenlists/compound.tokenlist.json"
  );

  const tokenList = JSON.parse(tokenListJson);

  return tokenList;
}

export async function marketContract(web3, cTokenName) {
  // TODO memoize
  const [addrJson, abiJson] = await Promise.all([
    fs.readFile("./abis/compound-mainnet.json"),
    fs.readFile("./abis/compound-mainnet-abi.json")
  ]);

  const mainnetAddr = JSON.parse(addrJson);
  const mainnetAbi = JSON.parse(abiJson);

  const contract = new web3.eth.Contract(
    mainnetAbi[cTokenName],
    mainnetAddr.Contracts[cTokenName]
  );

  return contract;
}

export async function borrowAndSupplyRate(web3, cTokenName, options = {}) {
  const contract = await marketContract(web3, cTokenName);

  let borrowRate, supplyRate;
  if ("daysAgo" in options) {
    const blockNum = await web3.eth.getBlockNumber();

    const blockNumWeekAgo = blockNum - BLOCKS_PER_DAY * 7;

    // NOTE we don't have access to archive nodes in infura
    [borrowRate, supplyRate] = await Promise.all([
      contract.methods.borrowRatePerBlock().call({}, blockNumWeekAgo),
      contract.methods.supplyRatePerBlock().call({}, blockNumWeekAgo)
    ]);
  } else {
    [borrowRate, supplyRate] = await Promise.all([
      contract.methods.borrowRatePerBlock().call(),
      contract.methods.supplyRatePerBlock().call()
    ]);
  }

  return { borrowRate, supplyRate };
}

export const rateToApy = rateInWei => {
  /* Somehow, APY rates don't depend on decimals of the contract
  //decimalsRes
  //contract.methods.decimals().call()
  */
  const mantissa = BN("10").pow(18);
  const daysPerYear = 365;

  return BN(rateInWei)
    .div(mantissa)
    .times(BLOCKS_PER_DAY)
    .plus(1)
    .pow(daysPerYear)
    .minus(1)
    .times(100);
};
